#fall.py
import math
import cv2
import numpy as np
import os
import sys
import time
import datetime

import argparse

parser = argparse.ArgumentParser(description='Process and record # of larvae dropping.')

parser.add_argument('-src', action='store',type=int, default=0, help="video stream source number, try 0 or 1, default is 0")
parser.add_argument('-x', type=int, nargs = 2, help="slices the frame, x values, horizontally")
parser.add_argument('-y', type=int, nargs = 2, help="slices the frame, y values, vertically")

parser.add_argument("-b", "--blurval", type=int, default=5, help="Gaussian Blur value, default is 5, I think it has to be an odd number.")
parser.add_argument("-t", "--threshval", type=int, default=20, help="B/W Threshold Value, default is 20.")
parser.add_argument("-c", "--contoursize", type=int, default = 50, help="Minimum contour size (pixels) to be detected, default is 50.")

parser.add_argument("-test", type=bool, default=0, help="Run in test mode: no images recorded. Default is false.")

#blurval = int(sys.argv[1])  	#5
#threshval = int(sys.argv[2])	#20
#contoursize = int(sys.argv[3])	#50

args = parser.parse_args()

stream = cv2.VideoCapture(args.src)
ret, test_frame = stream.read()
test_size = test_frame.shape
fheight = test_size[0]
fwidth = test_size[1]

slice_height_1 = 0
slice_width_1 = 0
slice_height_2 = fheight
slice_width_2 = fwidth

if(args.x is not None):
	slice_width_1 = args.x[0]
	slice_width_2 = args.x[1]

if(args.y is not None):
	slice_height_1 = args.y[0]
	slice_height_2 = args.y[1]


text_file = open("output.txt", "w")

firstFrame = None

t0 = time.time()


print("Arguments--------")
print("Source Num: {}".format(args.src))
print("X Slice: {}".format("no" if args.x is None else (str(args.x[0]) + " to " + str(args.x[1]))))
print("Y Slice: {}".format("no" if args.y is None else (str(args.y[0]) + " to " + str(args.y[1]))))
print("Blurval: {}".format(args.blurval))
print("Threshval: {}".format(args.threshval))
print("Contour Size: {}".format(args.contoursize))


while True:

	ret, frame = stream.read()

	if frame is None:
		print("Frame is none, can't read.")
		break


	frame = frame[slice_height_1:slice_height_2,slice_width_1:slice_width_2] 
	frame_copy = frame.copy()

	#change as needed
	
	timestamp = time.time()-t0
	currtime = datetime.datetime.now().strftime("%I-%M-%S-%f %p")

	# resize the frame, convert it to grayscale, and blur it
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	gray = cv2.GaussianBlur(gray, (args.blurval, args.blurval), 0)
	#gray_copy = gray.copy()

	# if the first frame is None, initialize it
	if firstFrame is None:
		firstFrame = gray
		continue

	# compute the absolute difference between the current frame and
	# first frame
	delta = cv2.absdiff(firstFrame, gray)
	thresh = cv2.threshold(delta, args.threshval, 255, cv2.THRESH_BINARY)[1]
	dilate = cv2.dilate(thresh, None, iterations=1)

	#first check: if there's no differences at all
	if cv2.countNonZero(dilate) == 0:
		firstFrame = gray
		continue

	_, cnts, _ = cv2.findContours(dilate.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

	contours = 0

	#count and draw contours
	for c in cnts:
		if cv2.contourArea(c) < args.contoursize:
			continue
		(r_x, r_y, r_w, r_h) = cv2.boundingRect(c)
		cv2.rectangle(frame_copy, (r_x, r_y), (r_x + r_w, r_y + r_h), (0, 255, 0), 2)
		contours +=1

	#second check - the contours aren't drawn or counted if they're not big enough
	if contours <= 0:
		firstFrame = gray
		continue

	cv2.putText(frame_copy, "Contours: {}".format(contours), (10, 20),
		cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
	cv2.putText(frame_copy, currtime,
		(10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
	
	cv2.imshow("Frame_edit", frame_copy)	
	cv2.imshow("Dilate", dilate)
	cv2.imshow("Delta", delta)

	if contours <= 3:
		print("> {:3} Contours found at {}".format(contours, currtime))
		
		if(args.test == 0):
				cv2.imwrite(os.path.join('images', currtime + ".jpg"), frame_copy)
				text_file.write("> {:3} Contours at {}\n".format(contours, currtime))

	firstFrame = gray

	key = cv2.waitKey(1) & 0xFF

	# if the `q` key is pressed, break from the loop
	if key == ord("q"):
		break

print("Done. Closing program.")
stream.release()
text_file.close()